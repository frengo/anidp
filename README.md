# An(other) IDP
ANIDP vuole essere un piccolo progetto che ha lo scopo di simulare un rudimentale servizio di Identity Provider basato su token.

Il progetto è composto da 2 componenti principali:

* [Server](#markdown-header-server)
* [Client](#markdown-header-client)

---

## Server
La componente server è un'applicazione Java creata con il framework Spring Boot.
E' scomponibile in due macro blocchi (moduli):

* [Identity Provider](#markdown-header-identity-provider)
* [Private resource](#markdown-header-private-resource)


### Identity Provider
E' il modulo server che fornisce i servizi di Identity Provider. Espone API REST in grado di:
* restituire un token a fronte di credenziali valide (login)
* validare un token precedentemente creato
* rimuovere un token (logout)

Il token generato viene salvato in memoria ed ha una durata di soli 60 secondi così da effettuare velocemente test relativi all'efficacia dell'API di controllo della validità dello stesso.
Seguono i dettagli dei servizi esposti.
 
 
 
#### POST /idp/token - Login
Esempio di chiamata per il recupero di un token:

    curl -X POST
    --header 'Content-Type: application/json'
    --header 'Accept: application/json'
    -d '{ \
      "username": "user", \
      "password": "password" \
      }'
    'http://localhost:8080/idp/token'


HTTP 200 - In caso di credenziali valide. Il token viene restituito in formato JSON nel body della response.

    {
      "token": "98041ef9-ee2a-4419-be4b-f43be5595ea3",
      "exp": "2018-05-19T07:21:33.108+0000"
    }

HTTP 401 - In caso di autenticazione fallita.
 
 
 
#### GET /idp/token/{token} - Validazione
Esempio di chiamata per la validazione di un token:

    curl -X GET
    --header 'Accept: application/json'
    'http://localhost:8080/idp/token/98041ef9-ee2a-4419-be4b-f43be5595ea3'

HTTP 200 - In caso di token trovato e valido. Nel body della response viene restituito il token per intero in formato JSON:

    {
      "token": "98041ef9-ee2a-4419-be4b-f43be5595ea3",
      "exp": "2018-05-19T07:21:33.108+0000"
    }

HTTP 404 - In caso di token non trovato

HTTP 401 - In caso di token non più valido
 
 
 
#### DELETE /idp/token/{token} - Logout
Esempio di chiamata per la rimozione di un token:

    curl -X DELETE
    'http://localhost:8080/token/b9a06b7d-d332-4480-a848-89a5ac38e99d'

HTTP 200 - In caso di token trovato e valido. Il body della response è vuoto

HTTP 404 - In caso di token non trovato

HTTP 401 - In caso di token non più valido
 
 
  
  
### Private resource
E' il modulo che ha lo scopo di simulare una risorsa privata accessibile esclusivamente fornendo un token valido in input.
Espone una sola API REST la quale restituisce il contenuto protetto solo se il token presente nello header della richiesta HTTP è valido.
Il token deve trovarsi all'interno dello header HTTP alla chiave Authorization:

    curl -X GET
    --header 'Authorization: c97fdd60-5b33-11e8-b566-0800200c9a66'
    'http://localhost:8080/private/myresource'

La validazione avviene invocando l'API esposta dal modulo IdP. Se questa valida il token, il servizio restituisce il contenuto protetto, viceversa restituisce un codice HTTP **401 Unauthorized** che indica la mancanza di permessi per accedere alla risorsa.

---

## Client
La componente client è una piccola pagina HTML che sfrutta la libreria jQuery per effettuare chiamate AJAX ai servizi REST esposti dalla componente server.
Nella pagina sono presenti dei bottoni che vengono abilitati in funzione dello stato del client.

**Stato 1** -
Il client non conosce il token. Viene quindi mostrata la form di login.

**Stato 2** -
L'utente inserisce le credenziali e prosegue con la login.
Il client invoca il servizio di login esposto da IdP e se la coppia username/password è valida, ottiene un token in output.
Il token viene salvato all'interno del **localStorage** del browser.
La pagina nasconde la form di login ed abilita i bottoni di logout e di chiamata al servizio privato.

**Stato 3** -
L'utente clicca il botone di logout. Il client invoca il servizio di logout esposto da IdP e cancella il token dal localStorage.
Il client si porta allo stato 1

**Note:** All'interno della pagina è sempre abilitato il bottone per forzare la chiamata al servizio esterno in quanto essendo un servizio esposto sul WEB, anche se l'utente non vede il bottone in pagina può comunque forzare la chiamata al servizio privato. E' quindi importante implementare una forma di autenticazione ed autorizzazione all'interno del servizio.


## Sequence Diagram
Viene qui mostrato il sequence diagram che meglio descrive il flusso di login e quello di invocazione del servizio privato.

![sequence](sequence_diagram.png)


## Compilazione ed esecuzione
L'applicazione usa MAVEN come strumento per la build.
Per procedere con la compilazione del progetto, clonare il respository git, spostarsi nella cartella server ed eseguire il comando:

    mvn install

La compilazione produce come output un file .jar, salvato nella directory target. Per eseguire l'applicazione eseguire il seguente comando:

    java -jar anidp-0.0.1-SNAPSHOT.jar

Il progetto in esecuzione espone i servizi HTTP sulla porta locale 8080.

Navigando all'url http://localhost:8080/swagger-ui.html è possibile accedere alla documentazione Swagger.
