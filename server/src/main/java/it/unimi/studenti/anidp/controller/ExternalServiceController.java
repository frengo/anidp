package it.unimi.studenti.anidp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import it.unimi.studenti.anidp.TokenStorage;
import it.unimi.studenti.anidp.model.Token;

@RestController	
@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/private")
public class ExternalServiceController {

	@Autowired
	TokenStorage tokenStorage;


    @RequestMapping(method=RequestMethod.GET, path="/myresource")
    @ResponseBody
    ResponseEntity<String> getResource(@RequestHeader("Authorization") String bearerToken) {
    	
    	if (bearerToken == null) {
    		throw new InvalidTokenException("token is null");
    	}
    	
    	bearerToken = bearerToken.replace("Bearer ", "");
    	
    	try {
        	RestTemplate restTemplate = new RestTemplate();
            Token token = restTemplate.getForObject("http://localhost:8080/idp/token/"+bearerToken, Token.class);

            if (token == null) {
            	throw new InvalidTokenException("token not found");
            }
            

    	} catch (HttpStatusCodeException e) {
    		if (e.getStatusCode().is4xxClientError()) {
    			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);	
    		}
    		
    		return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
    		
    	}
    	
    	return new ResponseEntity<String>("Here your protected resource!", HttpStatus.OK);

    }

}

@SuppressWarnings("serial")
@ResponseStatus(code=HttpStatus.UNAUTHORIZED)
class InvalidTokenException extends RuntimeException {
	public InvalidTokenException(String m) {
		super(m);
	}
}