package it.unimi.studenti.anidp.model;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Token {

	String token;
	Date exp;
	
	public Token() {
		this.token = UUID.randomUUID().toString();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.SECOND, 60);
		this.exp = c.getTime();
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExp() {
		return exp;
	}

	public void setExp(Date exp) {
		this.exp = exp;
	}

}
