package it.unimi.studenti.anidp;

import org.springframework.stereotype.Service;

import it.unimi.studenti.anidp.model.Token;

@Service
public interface TokenStorage {

	boolean addToken(Token t);
	boolean revokeToken(String t);
	Token getToken(String t);
	boolean isValid(String t);
}
