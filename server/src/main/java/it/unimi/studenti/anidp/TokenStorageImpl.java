package it.unimi.studenti.anidp;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import it.unimi.studenti.anidp.model.Token;

@Component
public class TokenStorageImpl implements TokenStorage {

	Map<String, Token> tokens = new HashMap<String, Token>();

	@Override
	public boolean addToken(Token t) {
		tokens.put(t.getToken(), t);
		return true;
	}


	@Override
	public boolean revokeToken(String t) {
		if (tokens.containsKey(t)) {
			
			boolean isValid = isValid(t); 
			tokens.remove(t);

			return isValid;
		}

		return false;
	}

	@Override
	public Token getToken(String t) {
		
		return tokens.get(t);
	}
	
	@Override
	public boolean isValid(String t) {
		if (t == null) {
			return false;
		}
		Token token = getToken(t);
		if (token == null) {
			return false;
		}
		
		return token.getExp().after(new Date());
	}
	
	
}
