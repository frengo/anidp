package it.unimi.studenti.anidp.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import it.unimi.studenti.anidp.App;
import it.unimi.studenti.anidp.TokenStorage;
import it.unimi.studenti.anidp.model.Token;
import it.unimi.studenti.anidp.model.User;

@RestController	
@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/idp")
public class TokenController {

	@Autowired
	TokenStorage tokenStorage;

    @RequestMapping(method=RequestMethod.POST, path="/token")
    @ResponseBody
    Token login(@RequestBody User u) {
    	
    	System.out.println("Autenticazione in corso..");

    	if (StringUtils.isEmpty(u.getPassword()) || StringUtils.isEmpty(u.getUsername())) {
    		throw new InvalidCredentialsException("mandatory fields not found");
    	}
    	
    	if (!App.checkUser(u.getUsername(), u.getPassword())) {
    		throw new InvalidCredentialsException("invalid credentials");
    	}
    	
    	
    	
    	Token t = new Token();
    	tokenStorage.addToken(t);
    	return t;
    }
    
    @RequestMapping(method=RequestMethod.GET, path="/token/{token}")
    @ResponseBody
    Token getToken(@PathVariable(name="token") String token) {

    	System.out.println("Richiesta validazione token " + token);
    	
    	Token t = tokenStorage.getToken(token);
    	if (t == null) {
    		System.out.println("token null");	
    		throw new TokenNotFounException("token not found");
    	}

    	if (t.getExp().before(new Date())) {
    		System.out.println("token scaduto");
    		throw new InvalidAppTokenException("token not valid");
    	}
    	
    	return t;
    }    
    
    @RequestMapping(method=RequestMethod.DELETE, path="/token/{token}")
    @ResponseBody
    void logout(@PathVariable(name="token") String token) {

    	System.out.println("Richiesta rimozione token " + token);

    	if (!tokenStorage.revokeToken(token)) {
    		throw new TokenNotFounException("token not found");
    	}
    	
    	return;
    }     

}

@SuppressWarnings("serial")
@ResponseStatus(code=HttpStatus.UNAUTHORIZED)
class InvalidCredentialsException extends RuntimeException {
	public InvalidCredentialsException(String m) {
		super(m);
	}
}

@SuppressWarnings("serial")
@ResponseStatus(code=HttpStatus.NOT_FOUND)
class InvalidAppTokenException extends RuntimeException {
	public InvalidAppTokenException(String m) {
		super(m);
	}
}

@SuppressWarnings("serial")
@ResponseStatus(code=HttpStatus.NOT_FOUND)
class TokenNotFounException extends RuntimeException {
	public TokenNotFounException(String m) {
		super(m);
	}
}